const messages = {
	en: {
		daily1: 'Hello again! Choose from 2 Wikipedia articles today. After reading, tap 👍 or 👎 to guide your future recommendations.',
		daily2: 'Welcome back! Here are 2 new Wikipedia articles. Once you’ve explored, don’t forget to give a 👍 or 👎',
		daily3: 'Ready for today’s discovery? We have 2 articles lined up. Let us know which one you like with a 👍 or don’t with a 👎',
		daily4: 'Another day, another set of reads! Check out these 2 articles and hit 👍 or 👎 to share your preference.',
		daily5: 'Today brings 2 new Wikipedia articles! Enjoy reading and use 👍 or 👎 to tell us what you think.',
		daily6: 'Six days in, and here are 2 more articles! Remember, your 👍 or 👎 helps us personalise your experience.',
		daily7: 'Your final set of articles for the week is here! Choose and react with a 👍 or 👎. We value your input!',
		'bot-welcome': 'Welcome to Read Wikipedia! 🌍 Your journey into the world of knowledge begins today. Simply select <b>Search Wikipedia</b> from the (☰) menu to read intriguing articles. I provide daily reading recommendations based on your search and reading.',
		'bot-reply': 'Thank you for reaching out! Interested in learning new knowledge? Simply select <b>Search Wikipedia</b> from the (☰) menu to read intriguing articles. I provide daily reading recommendations based on your search and reading. Please note that I\'m not designed for extended conversations at the moment.',
		'bot-search-instructions': 'Use the button below to find articles to read on Wikipedia.',
		'bot-search-button': 'Search Wikipedia',
		'bot-unsubscribe': 'Thank you for using Read Wikipedia! 🙏 You’ve been unsubscribed from daily articles. Remember, you can always resume receiving interesting reads anytime by selecting /start in the menu (☰). We’ll be here whenever you’re ready to explore more! 🌍 📚',
		'email-restricted': 'Currently restricted to WMF emails.',
		'email-duplicate': 'Email address already in used',
		'email-error': 'Unexpected server error',
		'email-registered': 'Thank you for signing up. Check your inbox for an email confirmation link.',
		'email-invalidlink': 'Invalid link',
		'email-notfound': 'Email address not found',
		'email-alreadyconfirmed': 'Your email is already confirmed.',
		'email-confirmsuccess': 'Thank you for confirming you email address. Your account is now active.<br>Use the <a href="$1">recommendation tool</a> to initialize your account with some articles you like.',
		'email-unsubscribe': 'Sorry to see you go. Your account was deleted.',
		'email-register-title': 'Welcome to the Wiki Subscription Service ✔',
		'botapp-title':  'Looking to read the <b>$1</b> article? Here it is! Also, starting tomorrow same time, you’ll get two Wikipedia articles related to this topic.',
		'botapp-title2':  'Looking to read the <b>$1</b> article? Here it is!',
		'bot-read': 'Read',
		'botinfo-name': 'Read Wikipedia',
		'botinfo-about': 'Daily Wikipedia article recommendations delivered to your Telegram! Discover fascinating reads with Read Wikipedia 🌍 Bot.',
		'botinfo-desc': 'Welcome to Read Wikipedia! 🌍 Receive 2 daily Wikipedia recommendations to effortlessly expand your knowledge. Say goodbye to endless scrolling and discover hidden gems. Our recommendation engine tailors suggestions based on your searches. 👍 to receive similar articles, 👎 to adjust preferences. Subscribe via /start, explore your 1st article with /search, and stop recommendations with /unsubscribe on the menu. Elevate your learning journey with us!.',
		'botinfo-command-start': 'Getting started',
		'botinfo-command-search': 'Search Wikipedia',
		'botinfo-command-unsubscribe': 'Stop receiving daily recommendations'
	},
	es: {
		daily1: '¡Hola de nuevo! Elija entre los siguientes dos artículos de Wikipedia hoy. Después de leer, toque 👍 o 👎 para guiar sus proximas recomendaciones.',
		daily2: '¡Bienvenido de nuevo! Aquí hay dos nuevos artículos de Wikipedia. Una vez que hayas explorado, no olvides dar un 👍 o 👎',
		daily3: '¿Listo para el descubrimiento de hoy? Tenemos dos artículos alineados. Cuéntanos cuál te gusta o no con un 👍 o 👎',
		daily4: '¡Otro día, otra serie de lecturas! Consulta estos dos artículos y presiona 👍 o 👎 para compartir tu preferencia.',
		daily5: '¡Hoy hay dos nuevos artículos de Wikipedia! Disfruta leyendo y usa 👍 o 👎 para decirnos lo que piensas.',
		daily6: '¡Seis días después y aquí hay dos artículos más! Recuerda, tu 👍 o 👎 nos ayuda a personalizar tu experiencia.',
		daily7: '¡Tu último par de artículos para la semana ya está aquí! Elige y reacciona con un 👍 o 👎. ¡Gracias y valoramos su opinión!',
		'bot-welcome': 'Bienvenido a Lee Wikipedia! 🌍 Tu viaje al mundo del conocimiento comienza hoy. Simplemente seleccione <b>Buscar en Wikipedia</b> en el menú (☰) para leer artículos interesantes. Yo te enviaré recomendaciones de artículos cada día basado en su búsqueda y lectura.',
		'bot-reply': '¡Gracias por comunicarte! ¿Estas interesado en aprender algo nuevo? Simplemente selecciona <b>Buscar en Wikipedia</b> en el menú (☰) para leer artículos interesantes. Yo te enviaré recomendaciones de artículos cada día basado en su búsqueda y lectura. Tenga en cuenta que no estoy diseñado para conversaciones prolongadas en este momento.',
		'bot-search-instructions': 'Use el boton de abajo para buscar articulos en Wikipedia.',
		'bot-search-button': 'Buscar en Wikipedia',
		'bot-unsubscribe': '¡Gracias por usar Leer Wikipedia! 🙏 Se te ha cancelado la suscripción a los artículos diarios. Recuerde, siempre puede continuar recibiendo lecturas interesantes en cualquier momento seleccionando /iniciar en el menú (☰). ¡Estaremos aquí cuando estés listo para explorar más! 🌍 📚',
		'email-restricted': 'Por los momentos solo está disponible para emails de WMF.',
		'email-duplicate': 'Este correo ya ha sido registrado',
		'email-error': 'Error de servidor inesperado',
		'email-registered': 'Gracias por registrarte. Revise su buzón para obtener un enlace de confirmación por correo electrónico.',
		'email-invalidlink': 'Enlace invalido',
		'email-notfound': 'Correo electrónico no fue encontrado',
		'email-alreadyconfirmed': 'Su correo ya ha sido confirmado.',
		'email-confirmsuccess': 'Gracias por confirmar su dirección de correo electrónico. Su cuenta ya está activa.<br>Utilice la <a href="$1">herramienta de recomendación</a> para inicializar su cuenta con algunos artículos que le gusten.',
		'email-unsubscribe': 'Lamentamos verte partir. Tu cuenta ha sido desactivada.',
		'email-register-title': 'Bienvenido al Wiki Subscription ✔',
		'botapp-title': '¿Quieres leer el artículo <b>$1</b>? ¡Aquí lo tienes! Además, a partir de mañana a la misma hora, recibirás dos artículos de Wikipedia relacionados a este tema.',
		'botapp-title2': '¿Quieres leer el artículo de <b>$1</b>? ¡Ahí tienes!',
		'bot-read': 'Leer',
		'botinfo-about': '¡Recomendaciones diarias de artículos de Wikipedia enviadas a tu Telegram! Descubra lecturas fascinantes con Lee Wikipedia 🌍 Bot.',
		'botinfo-desc': '¡Bienvenido a Lee Wikipedia! 🌍 Reciba 2 recomendaciones diarias de Wikipedia para ampliar sus conocimientos sin esfuerzo. Di adiós al scrolling sin fin y descubre joyas de conocimiento ocultas. Nuestro motor de recomendaciones adapta las sugerencias según tus búsquedas. Oprima 👍 para recibir artículos similares, o 👎 para ajustar preferencias. Suscríbase a través de /start, explore su primer artículo con /search y detenga las recomendaciones con /unsubscribe en el menú. ¡Eleva tu aprendizaje con nosotros!',
		'botinfo-name': 'Lee Wikipedia',
		'botinfo-command-start': 'Para empezar',
		'botinfo-command-search': 'Buscar en Wikipedia',
		'botinfo-command-unsubscribe': 'Detener las recomendaciones de cada día'
	}
};

const validateLang = lang => {
	return [ 'en', 'es' ].indexOf( lang ) !== -1 ? lang : 'en';
}

export default ( lang, msg, params=[] ) => {
	lang = validateLang( lang );
	let str = messages[ lang ][ msg ] || messages.en[ msg ] || msg;
	for (let i = 0; i < params.length; i++) {
		str = str.replaceAll( '$' + ( i+1 ), params[ i ] );
	}
	return str;
}
