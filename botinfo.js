import TelegramBot from 'node-telegram-bot-api';
import i18n from './i18n.js';

const token = process.env.TELEGRAM_TOKEN || 'YOUR_TELEGRAM_BOT_TOKEN';
const bot = new TelegramBot(token, { polling: false });

for ( let lang of [ 'en', 'es' ] ) {
	await bot.setMyName( {
		name: i18n( lang, 'botinfo-name' ),
		language_code: lang
	} );
	await bot.setMyShortDescription( {
		name: i18n( lang, 'botinfo-about' ),
		language_code: lang
	} );
	await bot.setMyDescription( {
		name: i18n( lang, 'botinfo-desc' ),
		language_code: lang
	} );
	await bot.setMyCommands( [
	  { command: 'start', description: i18n( lang, 'botinfo-command-start' ) },
	  { command: 'search', description: i18n( lang, 'botinfo-command-search' ) },
	  { command: 'unsubscribe', description: i18n( lang, 'botinfo-command-unsubscribe' ) }
	], { language_code: lang } );
}
