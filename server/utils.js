import url from 'url';
import i18n from '../i18n.js';

export async function getSummary( title, lang ) {
	const url = 'https://' + lang + '.wikipedia.org/api/rest_v1/page/summary/' + encodeURIComponent( title );
	const response = await fetch( url );
	if ( response.status !== 200 ) {
		return null;
	}
	const summary = await response.json();
	const defaultImage = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Wikipedia-logo-v2-square.svg/480px-Wikipedia-logo-v2-square.svg.png';
	summary.thumbnail = summary.thumbnail || { source: defaultImage };
	summary.url = summary.content_urls?.mobile?.page || 'https://' + lang + '.wikipedia.org/wiki/' + encodeURIComponent( title );
	return summary;
}

export async function recordVotes( user, titles, votes, lang='en' ) {
	return await fetch( 'https://wiki-sub-rec.toolforge.org/votes', {
		method: 'post',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify( { user, lang, titles, votes } )
	} );
}

function truncate( str ) {
	const MAX_LENGTH = 200;
	return ( str.length > MAX_LENGTH ) ?
		str.slice( 0, MAX_LENGTH-3 ) + '...' :
		str;
}

// this only used by email message
export function getUnsubscribeLink ( { email, token, lang } ) {
	const urlParts = {
		protocol: 'https',
		hostname: 'wiki-subscription.toolforge.org',
		query: { email, token, lang },
		pathname: 'unsubscribe'
	};
	return url.format( urlParts );
}

export async function sendArticleSummary( bot, chatId, summary, allowVoting, lang ) {
	if ( !summary ) {
		return;
	}
	const buttons = [];
	if ( allowVoting ) {
		buttons.push( { text: '👍', callback_data: 'u_' + summary.title } );
		buttons.push( { text: '👎', callback_data: 'd_' + summary.title } );
	}
	buttons.push( { text: i18n( lang, 'bot-read' ), url: summary.url } );
	bot.sendPhoto( chatId, summary.thumbnail.source, {
		caption: `<b>${summary.title}</b>\n` + truncate( summary.extract ),
		parse_mode: 'HTML',
		reply_markup: {
			inline_keyboard: [ buttons ]
		}
	} );
}

