import crypto from 'crypto';
import url from 'url';
import express from 'express';
import ejs from 'ejs';
import expressLayouts from 'express-ejs-layouts';
import mailer from './email.js';
import Subscription from './sub.model.js';
import bot from './bot.js';
import { recordVotes, getSummary, sendArticleSummary, getUnsubscribeLink } from './utils.js';
import i18n from '../i18n.js';

const app = express();
const PORT = parseInt(process.env.PORT, 10) || 5173 ; // IMPORTANT!! You HAVE to use this environment variable as port!

app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(expressLayouts);

app.get( '/', ( req, res ) => {
    res.render( 'index' );
} );

app.post( '/register', async ( req, res ) => {
    const { email, lang } = req.body;

    // Make sure it is allowed
    if ( !email.match( /.*?@wikimedia\.org$/ ) ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-restricted' )
        } );
    }

    // Does it already exist?
    let sub = await Subscription.findOne( {
        where: { transportId: email }
    } );
    if ( sub ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-duplicate' )
        } );
    }

    // Must be good, create the account
    const token = crypto.randomUUID().replace(/-/g, '')
    const extra = JSON.stringify( { token } )
    sub = await Subscription.create( {
        transportType: 'email',
        transportId: email,
        active: false,
        lang,
        extra
    } );
    if ( !sub ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-error' )
        } );
    }
    const msg = await createRegisterEmail( email, token, lang );
    await mailer.send( msg );
    return res.render( 'message', {
        type: 'success',
        message: i18n( lang, 'email-registered' )
    } );
} );


app.get('/confirm', async (req, res) => {
    const { email, token, lang } = req.query;

    if ( !email || !token ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-invalidlink' )
        } );
    }

    // Does it exist?
    let sub = await Subscription.findOne({
        where: { transportId: email }
    });
    if ( !sub ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-notfound' )
        } );
    }

    // Is it already active?
    if ( sub.active ) {
        return res.render( 'message', {
            type: 'success',
            message: i18n( lang, 'email-alreadyconfirmed' )
        } );
    }

    // Is the secret token valid?
    if ( sub.getToken() !== token ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-invalidlink' )
        } );
    }

    // All good, activate the account
    await sub.activate();
    const url = `https://wiki-sub-rec.toolforge.org/?lang=${sub.lang}&user=${sub.id}`;
    return res.render( 'message', {
        type: 'success',
        message: i18n( lang, 'email-confirmsuccess', [ url ] )
    } );
} );

app.get('/unsubscribe', async (req, res) => {
    const { email, token, lang } = req.query;

    if ( !email || !token ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-invalidlink' )
        } );
    }

    // Does it exist?
    let sub = await Subscription.findOne({
        where: { transportId: email }
    });
    if ( !sub ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-notfound' )
        } );
    }

    // Is the secret token valid?
    if ( sub.getToken() !== token ) {
        return res.render( 'message', {
            type: 'error',
            message: i18n( lang, 'email-invalidlink' )
        } );
    }

    // All good, delete the account
    await sub.destroy();
    return res.render( 'message', {
        type: 'success',
        message: i18n( lang, 'email-unsubscribe' )
    } );
} );

async function createRegisterEmail( to, token, lang ) {
    const msg = {
        from: 'wiki-subscription.noreply@toolforge.org',
        to,
        subject: i18n( lang, 'email-register-title' )
    };

    const urlParts = {
        protocol: 'https',
        hostname: 'wiki-subscription.toolforge.org',
        query: { email: to, token, lang },
        pathname: 'confirm'
    };
    const confirmationLink = url.format( urlParts );
    const unsubscriptionLink = getUnsubscribeLink( { email: to, token, lang });

    urlParts.query = null;
    urlParts.pathname = 'privacy';
    const privacyLink = url.format( urlParts );

    [ 'html', 'text' ].forEach( async ( format ) => {
        msg[ format ] = await ejs.renderFile(
            `./views/email_register.${format}.ejs`,
            {
                confirmationLink,
                unsubscriptionLink,
                privacyLink
            }
        );
    } );
    return msg;
}

app.get( '/privacy', ( req, res ) => {
    return res.render( 'privacy' );
} );

app.get('/botapp/selectarticle', async ( req, res ) => {
  const { queryId, userId, title, chatId, lang } = req.query;

  // Check if the account already exist
  let sub = await Subscription.findOne( {
      where: {
        transportId: chatId,
        transportType: 'telegram'
      }
  } );

  // Create the account if it doesn't already exist
  let titleMessage;
  if ( !sub ) {
    sub = await Subscription.create( {
        transportType: 'telegram',
        transportId: chatId,
        active: true,
        lang
    } );
    titleMessage = 'botapp-title';
  } else {
    // Make sure the account is active
    await sub.activate();
    titleMessage = 'botapp-title2';
  }

  bot.sendMessage(
    chatId,
    i18n( lang, titleMessage, [ title ] ),
    { parse_mode: 'HTML' }
  );
  const summary = await getSummary( title, lang );
  sendArticleSummary( bot, chatId, summary, false, lang );

  // Send selected title to rec engine
  await recordVotes( sub.id, [ title ], {}, lang );

  return res.send( 'OK' );
} );

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
