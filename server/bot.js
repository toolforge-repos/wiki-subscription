import TelegramBot from 'node-telegram-bot-api';
import Subscription from './sub.model.js';
import { recordVotes } from './utils.js';
import i18n from '../i18n.js';

const token = process.env.TELEGRAM_TOKEN || 'YOUR_TELEGRAM_BOT_TOKEN';
const bot = new TelegramBot(token, { polling: true, onlyFirstMatch: true } );
const APP_URL = 'https://wiki-subscription.toolforge.org/botapp.html';

bot.onText(/\/start/, ( msg, match ) => {
  bot.sendMessage(
    msg.chat.id,
    i18n( msg.from.language_code, 'bot-welcome' ),
    { parse_mode: 'HTML' }
  );
} );

bot.onText(/\/search/, ( msg, match ) => {
  bot.sendMessage( msg.chat.id, i18n( msg.from.language_code, 'bot-search-instructions' ), {
    parse_mode: 'HTML',
    reply_markup: {
      one_time_keyboard: true,
      inline_keyboard: [
        [
          { text: i18n( msg.from.language_code, 'bot-search-button') , web_app: { url:  APP_URL + '?chatId=' + msg.chat.id + '&lang=' + msg.from.language_code } }
        ]
      ]
    }
  } );
} );

bot.onText(/\/unsubscribe/, async ( msg, match ) => {
  bot.sendMessage(
    msg.chat.id,
    i18n( msg.from.language_code, 'bot-unsubscribe' ),
    { parse_mode: 'HTML' }
  );
  const sub = await Subscription.findOne( {
      where: {
        transportId: msg.chat.id,
        transportType: 'telegram'
      }
  } );
  if ( sub ) {
    await sub.destroy();
  }
} );

bot.onText( /.+/, async ( msg, match ) => {
    bot.sendMessage(
      msg.chat.id,
      i18n( msg.from.language_code, 'bot-reply' ),
      { parse_mode: 'HTML' }
    );
} );

bot.on('callback_query', async ( callbackQuery ) => {
  const msg = callbackQuery.message;
  const action = callbackQuery.data[0];
  const title = callbackQuery.data.slice( 2 );
  if ( action === 'u' || action === 'd' ) {
    const newKb = msg.reply_markup.inline_keyboard[0].filter( k => k.url );
    bot.editMessageReplyMarkup( {
      inline_keyboard: [ newKb ] },
      {
        chat_id: msg.chat.id,
        message_id: msg.message_id
      }
    );

    let sub = await Subscription.findOne( {
        where: {
          transportId: msg.chat.id,
          transportType: 'telegram'
        }
    } );
    if ( sub ) {
      const votes = {};
      votes[ title ] = { u: 1, d: -1 }[ action ];
      recordVotes( sub.id, [], votes, sub.lang );
    }
  }
} );

export default bot;
