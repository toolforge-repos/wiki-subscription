import nodemailer from "nodemailer";

const transporter = nodemailer.createTransport({
    host: "mail.tools.wmcloud.org",
    port: 587,
    secure: false,
    logger: false,
    debug: false
});

const send = ( message ) => {
    return transporter.sendMail( message );
}

export default { send }
