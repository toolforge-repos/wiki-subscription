import { Sequelize, DataTypes } from 'sequelize';
import PropertiesReader  from 'properties-reader';

const properties = PropertiesReader( process.argv[2] );
const host = properties.get('client.host') || 'tools.db.svc.wikimedia.cloud';
const user = properties.get('client.user');
const password = properties.get('client.password');
const db = properties.get('client.db') || user + '__wikisubscription';

const sequelize = new Sequelize(
   db,
   user,
   password,
    {
      host,
      dialect: 'mysql',
      pool: {
          max: 5,
          min: 0,
          acquire: 30000,
          idle: 10000
        }
    }
  );

const Subscription = sequelize.define( 'wiki_subs', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  active: {
    type: DataTypes.BOOLEAN,
    allowNull: false
  },
  transportType: {
    type: DataTypes.STRING,
    allowNull: false
  },
  transportId: {
    type: DataTypes.STRING(320),
    allowNull: false
  },
  lang: {
    type: DataTypes.STRING(10),
    allowNull: false,
    defaultValue: 'en'
  },
  extra: {
    type: DataTypes.TEXT
  }
} );

Subscription.prototype.getToken = function () {
  return JSON.parse( this.extra || '{}' ).token;
};

Subscription.prototype.activate = function () {
  if ( !this.active ) {
    this.active = true;
    return this.save();
  }
};

Subscription.prototype.deactivate = function () {
  if ( this.active ) {
    this.active = false;
    return this.save();
  }
};

await Subscription.sync( { alter: true } );

export default Subscription;
