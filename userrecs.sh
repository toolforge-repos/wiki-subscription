#!/bin/bash

# usage:
#    toolforge jobs run test --command "www/js/userrecs.sh <user id>" --image node18

cd www/js/
TELEGRAM_TOKEN=`cat ../../TELEGRAM_TOKEN` node ./recs.js ../../replica.my.cnf $1
