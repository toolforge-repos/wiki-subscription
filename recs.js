import ejs from 'ejs';
import TelegramBot from 'node-telegram-bot-api';
import Subscription from './server/sub.model.js';
import { getSummary, sendArticleSummary, recordVotes, getUnsubscribeLink } from './server/utils.js';
import mailer from './server/email.js';
import i18n from './i18n.js';

const token = process.env.TELEGRAM_TOKEN || 'YOUR_TELEGRAM_BOT_TOKEN';
const bot = new TelegramBot(token, { polling: false });

const getMessage = ( lang, day ) => {
    return i18n( lang, 'daily' + ( ( ( day - 1 ) % 7 ) + 1 ) );
};

// TODO - how to seed rec engine with initial title when delivering via email?
// Currently just using a random tile from randomTitles
const randomTitles = [ 'Rawayana', 'Cat', 'Poland', 'Sun'];

const computeSubDays = sub => {
    const now = Date.now();
    const delta = now - Date.parse(sub.createdAt);
    const days = Math.floor( ( delta / ( 60*60*24*1000 ) ) ) + 1;
    sub.day = days;
};

const getSubscribersByHour = async ( hour ) => {
    const subs = await Subscription.findAll( {
        where: { active: true }
    } );

    // Filter for subscribers whose registration hour matches current hour
    const currentHourSubs = subs.filter( sub => {
        const registeredHour = sub.createdAt.getUTCHours();
        return hour === registeredHour;
    });

    return currentHourSubs;
};

const getSubscribers = async () => {
    const userId = process.argv[3];
    const currentHour = new Date().getUTCHours();
    let subs;
    if ( userId ) {
        const sub = await Subscription.findByPk( parseInt( userId ) );
        subs = sub ? [ sub ] : [];
    } else {
        subs = await getSubscribersByHour( currentHour );
    }
    subs.forEach( computeSubDays );
    return subs;
}

const getRecs = async ( user, lang, n ) => {
    const response = await fetch( `https://wiki-sub-rec.toolforge.org/recs?n=${n}&lang=${lang}&user=${user}` );
    const text = await response.text();
    try {
        const json = JSON.parse( text );
        if ( json ) {
            return Promise.all( json.map( rec => getSummary( rec, lang ) ).filter( s => s) );
        }
    } catch {
        console.log( 'Invalid recs for user ', user, "\n", text, "\n" );
    }
    return [];
};

const createMsg = async ( sub, recs ) => {
    const msg = {
        from: 'wiki-subscription.noreply@toolforge.org',
        to: sub.transportId,
        subject: 'Your daily Wikipedia reading recommendations'
    };

    const unsubscriptionLink = getUnsubscribeLink( {
        email: sub.transportId,
        token: sub.getToken(),
        lang: sub.lang
    } );

    [ 'html', 'text' ].forEach( async ( format ) => {
        msg[ format ] = await ejs.renderFile(
            `./views/daily.${format}.ejs`,
            {
                msg: getMessage( sub.lang, sub.day ),
                recs: recs,
                unsubscriptionLink
            }
        );
    } );

    return msg;
}

const sendRecsEmail = async ( sub, recs ) => {
    const msg = await createMsg( sub, recs );
    await mailer.send( msg );
}

const sendRecsTelegram = async ( sub, recs ) => {
    try {
        await bot.sendMessage( sub.transportId, getMessage( sub.lang, sub.day ) );
        for ( let rec of recs ) {
            await sendArticleSummary( bot, sub.transportId, rec, true, sub.lang );
        }
    } catch ( err ) {
        console.log( 'Failed to send recs to telegram user ', sub.transportId, err.message );
    }
}

const getRecsReport = ( recs ) => {
    return `[ ` +  recs.map( r => r.title ).join( ',' ) + ` ]`;
}

const getDateReport = ( createdAt ) => {
    return new Date( createdAt ).toUTCString();
}

async function init () {
    const subscribers = await getSubscribers();

    let report = '\n######################### CRON JOB REPORT #########################\n';
    report += `Number of subscribers: ${subscribers.length}\n`;

    
    for ( const subscriber of subscribers ) {
        if (subscriber.day > 7) {
            // TODO: uncomment line below when running the experiment
            // return;
        }

        if ( subscriber.transportType === 'email' ) {
            // Initialize user on server side
            const initialTitle = randomTitles[ Math.floor( Math.random() * randomTitles.length ) ];
            await recordVotes( subscriber.id, [ initialTitle ], {} );
        }

        // Get recommendations
        const recs = await getRecs( subscriber.id, subscriber.lang, 2 );
        if ( recs.length === 0 ) {
            console.log( 'No recs for user ', subscriber.id );
            continue;
        }

        report += `id: ${subscriber.id}, transportId: ${subscriber.transportId}, transportType: ${subscriber.transportType}, createdAt: ${getDateReport(subscriber.createdAt)}, day: ${subscriber.day}, recs: ${getRecsReport(recs)}\n`;

        // Send recs according to transportType
        if ( subscriber.transportType === 'email' ) {
            await sendRecsEmail( subscriber, recs );
        } else if ( subscriber.transportType === 'telegram' ) {
            await sendRecsTelegram( subscriber, recs );
        }
    }
    report += '###################################################################\n'
    console.log(report);
}

init();
